# 环境搭建

## 任务要求：
1. 下载并安装[workbench](https://dev.mysql.com/downloads/workbench/)
2. 学习Workbanch的使用：
    * [How to Connect to a Database with MySQL Workbench](https://www.inmotionhosting.com/support/website/connect-database-remotely-mysql-workbench/)
    * [MySQL Workbench使用教程](http://c.biancheng.net/view/2625.html)
3. 使用Workbanch链接到远程的服务器。
4. 在远程服务器创建数据库，命名为：student_management_sys_[your name]

## 你的实现：
 请将你的实现结果截图到下面，提交截图。
